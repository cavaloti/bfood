import { get } from 'axios';
import {
  CITIES_FETCHING,
  CITIES_FETCHED_SUCCESS,
  CITIES_FETCHED_ERROR,
  SET_CITY_ID,
  CITY_RESULTS_FETCHING,
  CITY_RESULTS_SUCCESS,
  CITY_RESULTS_ERROR,
  CITY_CUISINES_FETCHING,
  CITY_CUISINES_SUCCESS,
  CITY_CUISINES_ERROR,
  SET_FILTER_CUISINE,
  SET_FILTER_COST,
  SET_FILTER_RATING,
  SET_FILTER_START,
  SET_FILTER_SORT,
  SET_FILTER_PAGE
} from '..';

const headers = {
  headers: {
    'user-key': '77576b2dae845bf32c1de0795a7753e1'
  }
};

export function getCuisinesByCityId() {
  return async (dispatch) => {
    dispatch({ type: CITY_CUISINES_FETCHING, payload: { loading: true } });
    const cityId = localStorage.getItem('cityId');
    await get(`${process.env.API_URL}/cuisines?city_id=${cityId}`, headers)
      .then(resp => resp.data)
      .then((data) => {
        dispatch({ type: CITY_CUISINES_SUCCESS, payload: data });
      })
      .catch((error) => {
        dispatch({ type: CITY_CUISINES_ERROR, payload: error });
      });
  };
}

export function filterByType(items, type, value, key = 'restaurant') {
  const filtered = items.filter((item) => {
    if (type === 'cost') {
      return (item[key].price_range <= value);
    }
    return Math.round(item[key].user_rating.aggregate_rating) <= value;
  });
  return filtered;
}

// TODO: improve filter search :/
export function getCityResults(cuisines = null, sort, count = 10, sortValue = null) {
  return async (dispatch, getState) => {
    dispatch({ type: CITY_RESULTS_FETCHING, payload: {} });
    const cityId = localStorage.getItem('cityId');
    const cuisinesTypes = cuisines ? `&cuisines=${cuisines}` : '';
    const orderBy = sortValue && sortValue < 4 ? 'asc' : 'desc';
    const { start } = getState().search;
    await get(`${process.env.API_URL}/search?entity_id=${cityId}&entity_type=city${cuisinesTypes}&start=${start}&count=${count}&sort=${sort}&order=${orderBy}`, headers)
      .then(resp => resp.data)
      .then((data) => {
        const valueOfType = getState().search[sort];
        if (valueOfType && sort) {
          data.restaurants = filterByType(data.restaurants, sort, valueOfType);
        }
        dispatch({ type: CITY_RESULTS_SUCCESS, payload: data });
      })
      .catch((error) => {
        dispatch({ type: CITY_RESULTS_ERROR, payload: error });
      });
  };
}

export function searchCities(query) {
  return async (dispatch) => {
    dispatch({ type: CITIES_FETCHING });
    await get(`${process.env.API_URL}/cities?q=${query}`, headers)
      .then(resp => resp.data)
      .then((data) => {
        dispatch({ type: CITIES_FETCHED_SUCCESS, payload: data });
      })
      .catch((error) => {
        dispatch({ type: CITIES_FETCHED_ERROR, payload: error });
      });
  };
}

export function setStart(start) {
  return (dispatch) => {
    dispatch({ type: SET_FILTER_START, payload: start });
  };
}

export function setPage(page) {
  return (dispatch) => {
    dispatch({ type: SET_FILTER_PAGE, payload: page });
  };
}

export function setCuisine(cuisine) {
  return (dispatch) => {
    dispatch(setPage(1));
    dispatch(setStart(1));
    dispatch({ type: SET_FILTER_CUISINE, payload: cuisine });
  };
}

export function setSort(sort) {
  return (dispatch) => {
    dispatch({ type: SET_FILTER_SORT, payload: sort });
  };
}

export function setCost(cost) {
  return (dispatch) => {
    dispatch(setPage(1));
    dispatch(setStart(1));
    dispatch({ type: SET_FILTER_COST, payload: cost });
  };
}

export function setRating(rating) {
  return (dispatch) => {
    dispatch(setPage(1));
    dispatch(setStart(1));
    dispatch({ type: SET_FILTER_RATING, payload: rating });
  };
}

export function setCity({ id, name }) {
  return (dispatch) => {
    localStorage.setItem('cityId', id);
    localStorage.setItem('cityName', name);
    dispatch({ type: SET_CITY_ID, payload: id });
  };
}
