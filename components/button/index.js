import styled from 'styled-components';
import { Button as MaterialButton } from '@material-ui/core';

const Button = styled(MaterialButton)`
  width: ${props => props.width || '200px'};
  border-radius: 120px !important;
  font-weight: bold !important;
`;

export default Button;
