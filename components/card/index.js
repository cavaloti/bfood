import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Button from '../button';
import Icons from '../icons';

const useStyles = makeStyles({
  root: {
    width: 345,
    textAlign: 'center',
    margin: 20,
    padding: 8
  },
  media: {
    height: 140,
    backgroundSize: 'auto'
  },
  actions: {
    textAlign: 'center',
    margin: '0 auto'
  }
});


const CardItem = ({
  name, thumb, address, votes, icon
}) => {
  const classes = useStyles();
  const Icon = Icons[icon];
  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={thumb || '/images/no-image.jpg'}
          title={name}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {name}
          </Typography>
          <Icon count={votes} />
          <Typography variant="body1">
            {address}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button className={classes.actions} size="large" color="primary" variant="contained">ORDER NOW</Button>
      </CardActions>
    </Card>
  );
};

CardItem.propTypes = {
  name: PropTypes.string,
  thumb: PropTypes.string,
  address: PropTypes.string,
  votes: PropTypes.number,
  icon: PropTypes.string
};

export default CardItem;
