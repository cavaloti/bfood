import { Container as MaterialContainer } from '@material-ui/core';
import styled from 'styled-components';

const Container = styled(MaterialContainer)`
  padding: 20px;
  background-color: ${props => props.background};
  color: ${props => props.color};  
`;

export default Container;
