import React from 'react';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faFacebook, faInstagram, faTwitter, faWhatsapp
} from '@fortawesome/free-brands-svg-icons';
import Container from '../container';
import GridContainer from '../grid-container';
import Title from '../title';
import TextBlock from '../text-block';

const useStyles = makeStyles(theme => ({
  section: {
    marginTop: 26,
    marginBottom: 26
  },
  socialMedia: {
    marginRight: 20
  },
  grid: {
    marginRight: 40
  },
  listItemLink: {
    textDecoration: 'none',
    fontSize: 18,
    color: theme.palette.secondary.main
  }
}));

const aboutItems = ['History', 'Our Team', 'Brand Guidelines', 'Terms & Condition', 'Privacy Policy'];
const servicesItems = ['How to Order', 'Our Product', 'Order Status', 'Promo', 'Payment Method'];
const OtherItems = ['Contact Us', 'Help', 'Privacy'];

const sections = (title, items) => {
  const classes = useStyles();
  return (
    <>
      <Title fontSize="30px" className={classes.section}>{title}</Title>
      {items.map((item, i) => <ListItem key={i}><a href="#" className={classes.listItemLink}>{item}</a></ListItem>)}
    </>
  );
};

const Footer = () => {
  const classes = useStyles();
  return (
  <GridContainer>
    <Container>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={5} className={classes.grid}>
          <Title fontSize="36px">Title Here</Title>
          <TextBlock fontSize="26px">
            Lorem ipsum dolor sit amet,
             consectetur adipiscing elit.
              Aliquam at dignissim nunc, id maximus ex.
               Etiam nec dignissim elit, at dignissim enim.
          </TextBlock>
          <a href="#" title="Instagram" className={classes.socialMedia}>
            <FontAwesomeIcon icon={faInstagram} size="2x" color="white"/>
          </a>
          <a href="#" title="Facebook" className={classes.socialMedia}>
            <FontAwesomeIcon icon={faFacebook} size="2x" color="white"/>
          </a>
          <a href="#" title="Twitter" className={classes.socialMedia}>
            <FontAwesomeIcon icon={faTwitter} size="2x" color="white"/>
          </a>
          <a href="#" title="Whatsapp" className={classes.socialMedia}>
            <FontAwesomeIcon icon={faWhatsapp} size="2x" color="white"/>
          </a>
        </Grid>
        <Grid item xs={12} sm={2}>
          <List>
            {sections('About', aboutItems)}
          </List>
        </Grid>
        <Grid item xs={12} sm={2}>
          <List>
            {sections('Services', servicesItems)}
          </List>
        </Grid>
        <Grid item xs={12} sm={2}>
          <List>
            {sections('Other', OtherItems)}
          </List>
        </Grid>
      </Grid>
    </Container>
  </GridContainer>
  );
};

const mapStateToProps = () => ({ });

export default (
  connect(
    mapStateToProps,
    null
  )(Footer)
);
