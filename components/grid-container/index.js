import Grid from '@material-ui/core/Grid';
import styled from 'styled-components';

const GridContainer = styled(Grid)`
  background-color: ${props => props.background || 'black'};
  padding-top: ${props => props.paddingTop};
  padding-bottom: 36px;
`;

export default GridContainer;
