import React from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Container from '../../container';
import GridContainer from '../../grid-container';
import Title from '../../title';
import TextBlock from '../../text-block';
import Button from '../../button';

const Header = () => (
  <GridContainer>
    <Container>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6}>
          <Title>Get Cashback <br/>up to 50%</Title>
          <TextBlock>
            Lorem ipsum dolor sit amet,
             consectetur adipiscing elit.
              Suspendisse consectetur justo eu nunc consequat.
          </TextBlock>
          <Button size="large" color="primary" variant="contained">ORDER NOW</Button>
        </Grid>
        <Grid item xs={12} sm={6}>
          <img src="/images/header-burger.jpg" title="Bfood burger" alt="Burger" />
        </Grid>
      </Grid>
    </Container>
  </GridContainer>
);

const mapStateToProps = () => ({ });

export default (
  connect(
    mapStateToProps,
    null
  )(Header)
);
