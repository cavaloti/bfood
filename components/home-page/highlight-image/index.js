import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const HighlightStyle = styled.img`
  margin-top: 40px;
  overflow: hidden;
  border-radius: 50%; 
`;

const HighlightImage = ({ src }) => <HighlightStyle src={src} />;

HighlightImage.propTypes = {
  src: PropTypes.string
};

export default HighlightImage;
