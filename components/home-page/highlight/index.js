import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Container from '../../container';
import GridContainer from '../../grid-container';
import Title from '../../title';
import TextBlock from '../../text-block';
import Button from '../../button';
import * as colors from '../../../styles/colors';
import HighlightImage from '../highlight-image';

const Highligth = ({ imageSrc, title, text }) => (
  <GridContainer background={colors.beige}>
    <Container >
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6}>
          <Title color={colors.red}>{title}</Title>
          <TextBlock color={colors.red}>{text}</TextBlock>
          <Button size="large" color="primary" variant="contained">ORDER NOW</Button>
        </Grid>
        <Grid item xs={12} sm={6}>
          <HighlightImage src={imageSrc} />
        </Grid>
      </Grid>
    </Container>
  </GridContainer>
);

Highligth.propTypes = {
  imageSrc: PropTypes.string,
  title: PropTypes.string,
  text: PropTypes.string
};

export default Highligth;
