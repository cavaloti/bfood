import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDollarSign } from '@fortawesome/free-solid-svg-icons';

const Dollar = ({ count }) => {
  const dollars = [];
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i <= count; i++) {
    // eslint-disable-next-line react/react-in-jsx-scope
    dollars.push(<FontAwesomeIcon key={i} icon={faDollarSign} color="#f4c323" style={{ margin: '3px', fontSize: '1.4em' }}/>);
  }
  return dollars;
};

Dollar.propTypes = {
  count: PropTypes.number
};

export default Dollar;
