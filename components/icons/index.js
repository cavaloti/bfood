const Icons = {};
Icons.star = require('../icons/star').default;
Icons.dollar = require('../icons/dollar').default;

export default Icons;
