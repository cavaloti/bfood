import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';

const Star = ({ count }) => {
  const stars = [];
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i <= count; i++) {
    // eslint-disable-next-line react/react-in-jsx-scope
    stars.push(<FontAwesomeIcon key={i} icon={faStar} color="#f4c323" style={{ margin: '3px', fontSize: '1.4em' }}/>);
  }
  return stars;
};

Star.propTypes = {
  count: PropTypes.number
};

export default Star;
