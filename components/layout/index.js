import React from 'react';
import PropTypes from 'prop-types';
import Menu from '../menu';
import './index.scss';

const Layout = ({ children }) => (
  <>
    <Menu />
    {children}
  </>
);

Layout.propTypes = {
  children: PropTypes.node
};

export default Layout;
