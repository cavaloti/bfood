import React from 'react';
import Grid from '@material-ui/core/Grid';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHamburger } from '@fortawesome/free-solid-svg-icons';
import Container from '../container';
import GridContainer from '../grid-container';

const links = [
  {
    label: 'HOME',
    href: '/'
  },
  {
    label: 'SEARCH',
    href: '#'
  },
  {
    label: 'PROMO',
    href: '#'
  },
  {
    label: 'ABOUT',
    href: '#'
  },
  {
    label: 'CONTACT',
    href: '#'
  }
];

const GridItems = styled(Grid)` display: flex; `;
const GridItem = styled(Grid)` 
margin-top: 15px;
margin-left: 20px;
  a {
    color: white;
    text-decoration: none;
    font-weight: bold;
    &:hover {
      opacity: 0.8
    }
  }
`;

const Menu = () => (
  <GridContainer>
    <Container>
      <Grid container spacing={2}>
        <GridItem item xs={12} sm={6}>
          <a href="/">
            <FontAwesomeIcon icon={faHamburger} size="3x" color="white"/>
          </a>
        </GridItem>
        <GridItems item xs={12} sm={6}>
          {links.map((link, index) => <GridItem key={index}>
            <a href={link.href}>{link.label}</a></GridItem>)};
        </GridItems>
      </Grid>
    </Container>
  </GridContainer>
);

export default Menu;
