import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Router from 'next/router';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '../../button';
import { searchCities, setCity } from '../../../actions/search';

class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      options: []
    };
    this.handleChange = this.handleChange.bind(this);
  }

  setOpen = () => this.setState({ open: !this.state.open })

  async handleChange(q) {
    const { search } = this.props;
    await this.props.searchCities(q);
    if (!search.cities.length) {
      return this.setState({ open: false });
    }
    this.setState({ open: true });
  }

  render() {
    const { search: { cities, loading } } = this.props;
    return (
      <>
        <Autocomplete
          id="search-cities"
          style={{ background: 'white', width: '50%', margin: '0 auto' }}
          open={this.state.open}
          onOpen={() => this.setOpen()}
          onClose={() => this.setOpen()}
          onChange={(event, value) => value && this.props.setCity(value)}
          getOptionSelected={(option, value) => option.name.includes(value.name)}
          getOptionLabel={option => option.name}
          options={cities}
          loading={loading}
          renderInput={params => (
            <TextField
              {...params}
              placeholder="Digite a cidade"
              color="secondary"
              variant="outlined"
              onChange={e => this.handleChange(e.target.value)}
              InputProps={{
                ...params.InputProps,
                endAdornment: (
                  <React.Fragment>
                    {loading ? <CircularProgress color="inherit" size={20} /> : null}
                    {params.InputProps.endAdornment}
                  </React.Fragment>
                )
              }}
            />
          )}
        />
        <Button
          size="large"
          color="secondary"
          variant="contained"
          onClick={() => Router.push('/results')}
          style={{ margin: '0 auto', marginTop: '20px', display: 'block' }}>
            SEARCH
          </Button>
      </>
    );
  }
}

SearchBar.propTypes = {
  search: PropTypes.object,
  setCity: PropTypes.func,
  loading: PropTypes.bool,
  searchCities: PropTypes.func
};

const mapStateToProps = ({ search }) => ({ search });

export default (
  connect(
    mapStateToProps,
    { searchCities, setCity }
  )(SearchBar)
);
