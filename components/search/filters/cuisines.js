import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Title from '../../title';
import * as colors from '../../../styles/colors';
import { getCuisinesByCityId, getCityResults, setCuisine } from '../../../actions/search';

class FilterCuisines extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cuisines: [],
      cuisine: ''
    };
    this.handleChange = this.handleChange.bind(this);
  }

  async componentDidMount() {
    await this.props.getCuisinesByCityId();
  }

  handleChange = (event) => {
    const { value } = event.target;
    const { type, count } = this.props.search;
    this.props.setCuisine(value);
    this.props.getCityResults(value, type, count);
    this.setState({ cuisine: value });
  };

  render() {
    const { search: { cuisines }, title } = this.props;
    return (
      <div>
        <Title fontSize="20px" color={colors.red} >{title}</Title>
        <FormGroup>
        <FormControl>
          <Select
              variant="outlined"
              color="primary"
              id="cuisines"
              value={this.state.cuisine}
              onChange={e => this.handleChange(e)}
            >
            {cuisines && cuisines.map((item, i) => (
            <MenuItem key={i} value={item.cuisine.cuisine_id}>{item.cuisine.cuisine_name}</MenuItem>
            ))}
          </Select>
        </FormControl>
        </FormGroup>
      </div>
    );
  }
}

FilterCuisines.propTypes = {
  title: PropTypes.string,
  type: PropTypes.string,
  search: PropTypes.object,
  cuisine: PropTypes.number,
  cuisines: PropTypes.array,
  setCuisine: PropTypes.func,
  getCityResults: PropTypes.func,
  getCuisinesByCityId: PropTypes.func
};

const mapStateToProps = ({ search }) => ({ search });

export default (
  connect(
    mapStateToProps,
    { getCuisinesByCityId, getCityResults, setCuisine }
  )(FilterCuisines)
);
