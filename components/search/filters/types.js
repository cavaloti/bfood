import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Title from '../../title';
import * as colors from '../../../styles/colors';
import Icons from '../../icons';
import {
  getCityResults, setRating, setCost, setSort
} from '../../../actions/search';

class FilterTypes extends React.Component {
  constructor(props) {
    super(props);
    const { type } = this.props;
    this.state = {
      [type + 1]: false,
      [type + 2]: false,
      [type + 3]: false,
      [type + 4]: false,
      [type + 5]: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.maxAverage = this.maxAverage.bind(this);
  }


  async componentDidUpdate(prevProps, prevState) {
    if (this.state !== prevState) {
      const { type } = this.props;
      const { cuisine, count } = this.props.search;
      const maxAverage = this.maxAverage();
      const propFuncName = `set${type.charAt(0).toUpperCase() + type.slice(1)}`;
      this.props.setSort(type);
      this.props[propFuncName](maxAverage);
      this.props.getCityResults(cuisine, type, count, maxAverage);
    }
  }

  maxAverage() {
    const { type } = this.props;
    const checkedTypes = Object.keys(this.state)
      .filter(name => this.state[type + name.slice(-1)] === true);
    const checkedNumbers = checkedTypes.length
      ? checkedTypes.map(item => Number(item.slice(-1)))
      : [0];
    const max = Math.max(...checkedNumbers);
    return max;
  }

  handleChange(event) {
    this.setState({ ...this.state, [event.target.name]: event.target.checked });
  }

  render() {
    const { title, type, icon } = this.props;
    const Icon = Icons[icon];
    const keys = Object.keys(this.state);
    return (
      <div>
        <Title fontSize="20px" color={colors.red} >{title}</Title>
        <FormGroup>
        {keys.map((key, i) => (
          <FormControlLabel
            key={i}
            control={
              <Checkbox
                checked={this.state[type + key]}
                onChange={this.handleChange}
                name={key}
                color="primary"
              />
            }
            label={<Icon count={i}/>}
          />
        ))}
        </FormGroup>
      </div>
    );
  }
}

FilterTypes.propTypes = {
  title: PropTypes.string,
  type: PropTypes.string,
  icon: PropTypes.string,
  search: PropTypes.object,
  cuisine: PropTypes.number,
  rating: PropTypes.number,
  cost: PropTypes.number,
  setRating: PropTypes.func,
  setNote: PropTypes.func,
  setSort: PropTypes.func,
  getCityResults: PropTypes.func
};

const mapStateToProps = ({ search }) => ({ search });

export default (
  connect(
    mapStateToProps,
    {
      getCityResults, setRating, setCost, setSort
    }
  )(FilterTypes)
);
