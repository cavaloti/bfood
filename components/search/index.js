import React from 'react';
import Grid from '@material-ui/core/Grid';
import Container from '../container';
import GridContainer from '../grid-container';
import Title from '../title';
import TextBlock from '../text-block';
import SearchBar from './bar';
import * as colors from '../../styles/colors';

const Search = () => (
  <GridContainer background={colors.red}>
    <Container >
      <Grid container spacing={2}>
        <Grid item xs={12} sm={12}>
          <Title align="center" color={colors.white}>Search the Best Restaurants</Title>
          <TextBlock align="center" color={colors.white}>
            Lorem ipsum dolor sit amet,
             consectetur adipiscing elit. <br />
             Vivamus lacinia odio vitae vestibulum vestibulum.
          </TextBlock>
          <SearchBar />
        </Grid>
      </Grid>
    </Container>
  </GridContainer>
);

export default Search;
