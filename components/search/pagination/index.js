import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Pagination from '@material-ui/lab/Pagination';
import { setStart, getCityResults, setPage } from '../../../actions/search';

class SearchPagination extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event, number) {
    const { cuisine, sort, count } = this.props.search;
    const pageStart = (number * count) - count;
    this.props.setPage(number);
    this.props.setStart(pageStart);
    this.props.getCityResults(cuisine, sort, count);
  }

  render() {
    // TODO: verify why api return results found but display only 100 items
    const { page, totalPages } = this.props.search;
    return (
      <div style={{ display: 'table', margin: '0 auto' }}>
        <Pagination count={totalPages} page={page} color="primary" onChange={this.handleChange} />
      </div>
    );
  }
}

SearchPagination.propTypes = {
  search: PropTypes.object,
  setStart: PropTypes.func,
  setPage: PropTypes.func,
  getCityResults: PropTypes.func
};

const mapStateToProps = ({ search }) => ({ search });

export default (
  connect(
    mapStateToProps,
    { setStart, getCityResults, setPage }
  )(SearchPagination)
);
