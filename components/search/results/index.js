import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import CardItem from '../../card';
import Loader from '../../loader';
import SearchPagination from '../pagination';
import { getCityResults } from '../../../actions/search';

class SearchResults extends React.Component {
  async componentDidMount() {
    await this.props.getCityResults();
  }

  parseVote = str => Math.round(parseInt(str, 0) - 1);

  render() {
    const { search: { restaurants, loading } } = this.props;
    const renderLoader = loading ? <Loader /> : null;
    if (!restaurants.length && !loading) {
      return <div>No results found</div>;
    }

    return (
      <>
        <div>
          {renderLoader}
          <SearchPagination />
        </div>
        <div style={{ display: 'flex', flexFlow: 'wrap' }}>
        {restaurants && restaurants.map((item, i) => (
            <CardItem
              key={i}
              icon="star"
              name={item.restaurant.name}
              thumb={item.restaurant.thumb}
              address={item.restaurant.location.address}
              votes={this.parseVote(item.restaurant.user_rating.aggregate_rating)}/>
        ))}
        </div>
      </>
    );
  }
}

SearchResults.propTypes = {
  search: PropTypes.object,
  restaurants: PropTypes.array,
  loading: PropTypes.bool,
  getCityResults: PropTypes.func
};

const mapStateToProps = ({ search }) => ({ search });

export default (
  connect(
    mapStateToProps,
    { getCityResults }
  )(SearchResults)
);
