import styled from 'styled-components';

const TextBlock = styled.p`
  color: ${props => props.color || 'white'};
  font-size: ${props => props.fontSize || '32px'};
  width: ${props => props.width || 'inherit'};
  text-align: ${props => props.align || 'left'};
`;

export default TextBlock;
