import styled from 'styled-components';

const Title = styled.h1`
  color: ${props => props.color || 'white'};
  font-size: ${props => props.fontSize || '64px'};
  font-weight: ${props => props.bold || 'normal'};
  text-align: ${props => props.align || 'left'};
`;

export default Title;
