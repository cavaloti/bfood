import React from 'react';
import { connect } from 'react-redux';
import Layout from '../components/layout';
import Header from '../components/home-page/header';
import Highlight from '../components/home-page/highlight';
import Search from '../components/search';
import Footer from '../components/footer';

const HomePage = () => (
    <Layout>
      <Header />
      <Highlight
        imageSrc="/images/highlight.jpg"
        title="Big Pizza"
        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lacinia odio vitae vestibulum vestibulum."
      />
      <Search />
      <Footer />
    </Layout>
);

const mapStateToProps = () => ({ });

export default (
  connect(
    mapStateToProps,
    null
  )(HomePage)
);
