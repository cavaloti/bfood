import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Container from '../components/container';
import GridContainer from '../components/grid-container';
import Layout from '../components/layout';
import Footer from '../components/footer';
import Title from '../components/title';
import * as colors from '../styles/colors';
import FilterTypes from '../components/search/filters/types';
import FilterCuisines from '../components/search/filters/cuisines';
import SearchResults from '../components/search/results';

const ResultsPage = () => {
  const [cityName, setCityName] = useState('');
  useEffect(() => {
    setCityName(localStorage.getItem('cityName'));
  });
  return (
    <Layout>
      <GridContainer background={colors.beige}>
        <Container>
          <Title fontSize="36px" color={colors.red} bold>RESULTS FOR: <span>{cityName}</span></Title>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={4}>
              <FilterTypes title="NOTE" type="rating" icon="star"/>
              <FilterTypes title="COST" type="cost" icon="dollar"/>
              <FilterCuisines title="CUISINES" type="cuisines"/>
            </Grid>
            <Grid item xs={12} sm={8}>
              <SearchResults />
            </Grid>
          </Grid>
        </Container>
      </GridContainer>
      <Footer />
    </Layout>
  );
};

const mapStateToProps = () => ({ });

export default (
  connect(
    mapStateToProps,
    null
  )(ResultsPage)
);
