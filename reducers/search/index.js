/* eslint-disable no-case-declarations */
import {
  CITIES_FETCHING,
  CITIES_FETCHED_SUCCESS,
  CITIES_FETCHED_ERROR,
  SET_CITY_ID,
  CITY_CUISINES_SUCCESS,
  CITY_RESULTS_FETCHING,
  CITY_RESULTS_SUCCESS,
  SET_FILTER_CUISINE,
  SET_FILTER_COST,
  SET_FILTER_RATING,
  SET_FILTER_SORT,
  SET_FILTER_START,
  SET_FILTER_PAGE
} from '../../actions';

const INITIAL_STATE = {
  loading: false,
  error: false,
  cityId: null,
  cityName: null,
  cities: [],
  cuisines: [],
  restaurants: [],
  cuisine: null,
  rating: null,
  cost: null,
  sort: null,
  count: 10,
  start: 0,
  page: 1,
  totalPages: 0
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case CITY_RESULTS_FETCHING:
      return { ...state, loading: true };
    case CITY_RESULTS_SUCCESS:
      const pages = Math.ceil(action.payload.results_found / 10);
      return {
        ...state,
        restaurants: [...action.payload.restaurants],
        totalPages: pages,
        loading: false
      };
    case CITIES_FETCHING:
      return { ...state, loading: true };
    case CITIES_FETCHED_SUCCESS:
      return { ...state, cities: [...action.payload.location_suggestions], loading: false };
    case CITIES_FETCHED_ERROR:
      return { ...state, error: true, loading: false };
    case SET_CITY_ID:
      return { ...state, cityId: action.payload.id };
    case SET_FILTER_CUISINE:
      return { ...state, cuisine: action.payload };
    case SET_FILTER_COST:
      return { ...state, cost: action.payload };
    case SET_FILTER_RATING:
      return { ...state, rating: action.payload };
    case SET_FILTER_SORT:
      return { ...state, sort: action.payload };
    case SET_FILTER_START:
      return { ...state, start: action.payload };
    case SET_FILTER_PAGE:
      return { ...state, page: action.payload };
    case CITY_CUISINES_SUCCESS:
      return { ...state, cuisines: [...action.payload.cuisines], loading: false };
    default:
      return state;
  }
}
