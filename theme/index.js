import { createMuiTheme } from '@material-ui/core/styles';

// Create a theme instance.
const theme = createMuiTheme({
  typography: {
    fontFamily: 'Helvetica'
  },
  palette: {
    primary: {
      main: '#e64e3c'
    },
    secondary: {
      main: '#fff'
    },
    error: {
      main: '#680000'
    },
    background: {
      default: 'red'
    }
  }
});

export default theme;
